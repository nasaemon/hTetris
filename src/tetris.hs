module Tetris where

import Data.List
import Data.Maybe
import System.Random

data Shape = J | L | I | S | Z | O | T
  deriving (Show, Enum, Eq)

data Block = Block {shape::Shape, moving::Bool, origin::Bool}
  deriving (Eq, Show)

type Row = [Maybe Block]
type Grid = [Row]

-- 定数
gridHeight = 26
gridWidth = 10

newGame :: Grid
newGame = replicate gridHeight $ replicate gridWidth Nothing

randomShape :: RandomGen g => g -> (Shape, g)
randomShape g = (toEnum r, g')
  where (r, g') = randomR (0, length [J ..] -1) g

update :: Grid -> Shape -> Grid
update = addBlock . clearLines . freezeBlocks

addBlock :: Grid -> Shape -> Grid
addBlock rows shape'
  | empty rows && not (gameOver rows) = createShape shape' ++ drop 4 rows
  | otherwise = rows

dropBlock :: Grid -> Grid
dropBlock rows
  | nrows /= rows = dropBlock nrows
  | otherwise = rows
  where nrows = gravitate rows

moveRight :: Grid -> Grid
moveRight rows
  | touchRight rows = rows
  | otherwise = transpose . gravitate . transpose $ rows -- 改善の余地あり

moveLeft rows
  | touchLeft rows = rows
  | otherwise = map reverse . transpose . gravitate . transpose . map reverse $ rows -- 改善の余地あり

touchRight :: Grid -> Bool
touchRight = any moving . mapMaybe last

touchLeft :: Grid -> Bool
touchLeft = any moving . mapMaybe head

rotate :: Grid -> Grid
rotate g = insertRotated (clearGrid g) (rotateBlock g) (map (getBlock g) (movingCoordinates g)) -- 改善の余地あり

insertRotated :: Grid -> [(Int, Int)] -> [Maybe Block] -> Grid
insertRotated grid [] _ = grid
insertRotated grid (h:t) (val:valt) = insertRotated (setBlock grid h val) t valt
insertRotated _ _ [] = error "Error: insertRotated"

clearGrid :: Grid -> Grid
clearGrid grid = clearGrid' grid $ movingCoordinates grid

clearGrid' :: Grid -> [(Int, Int)] -> Grid
clearGrid' = foldl (\grid h -> setBlock grid h Nothing)

movingCoordinates :: Grid -> [(Int, Int)]
movingCoordinates [] = []
movingCoordinates (h:t) = movingCoordinates' h (25 - length t) ++ movingCoordinates t

movingCoordinates' :: Row -> Int -> [(Int, Int)]
movingCoordinates' [] _ = []
movingCoordinates' (h:t) y
  | movingBlock h = (y, 9 - length t): movingCoordinates' t y
  | otherwise = movingCoordinates' t y

getOrigin :: Grid -> (Int, Int)
getOrigin = head .origins

isOrigin :: Grid -> (Int, Int) -> Bool
isOrigin grid (x, y) = maybe False origin $ getBlock grid (x, y)

origins :: Grid -> [(Int, Int)]
origins grid = filter (isOrigin grid) (movingCoordinates grid)

rotateBlock :: Grid -> [(Int, Int)]
rotateBlock grid
  | hasOrigin grid && all (unoccupied grid) rotated = rotated
  | otherwise = moving_coords
  where
    moving_coords = movingCoordinates grid
    rotated = map (rotatePoint $ getOrigin grid) moving_coords

rotatePoint (originx, originy) (x, y) = (originx + originy - y, originy + originx + x)

hasOrigin = not . null . origins

unoccupied grid (x, y) = and [x > 0, x < gridHeight, y>0, y < gridWidth, not . stationoryBlock $ getBlock grid (x, y)]

getBlock grid (x, y) = grid !! x !! y

setBlock grid (x,y) val = g1 ++ setBlock' (head g2) y val : tail g2
  where (g1, g2) = splitAt x grid

setBlock' row y val = r1 ++ val : tail r2
  where (r1, r2) = splitAt y row

score = product . replicate 2 . length . filter id . map fullLine
gameOver = any (not . all moving . catMaybes) . take 4

gravitate rows
  | stopped rows = rows
  | otherwise = transpose . gravitate_rows . transpose $ rows
  where
    gravitate_rows [] = []
    gravitate_rows (h:t) = gravitate_row h : gravitate_rows t

    gravitate_row [] = []
    gravitate_row row@(h:t)
      | movingBlock h = move_blocks row
      | otherwise = h : gravitate_row t

move_blocks l
  | is_gap (gap l) = (Nothing:movingBlocks l) ++ tail (gap l) ++ ground l
  | otherwise = error "Error: move_blocks"
  where
    is_gap row = not (null $ gap row) && isNothing (head $ gap row)

    movingBlocks (h:t) | movingBlock h = h:movingBlocks t
    movingBlocks _  = []

    gap (Nothing:t) = Nothing:gap' t
    gap (h:t) | movingBlock h = gap t
    gap _ = []

    gap' (Nothing:t) = Nothing:gap' t
    gap' _ = []

    ground [] = []
    ground (h:t)
      | stationoryBlock h = h:t
      | otherwise = ground t

stopped rows = any stopped' (transpose rows) || empty rows
  where
    stopped' [] = False
    stopped' row | all movingBlock row = True
    stopped' (fir:sec:_) | movingBlock fir && stationoryBlock sec = True
    stopped' (_:t) = stopped' t

movingBlock :: Maybe Block -> Bool
movingBlock = maybe False moving
stationoryBlock = maybe False (not . moving)

empty rows = all empty' (transpose rows)
  where empty' l = not (any moving (catMaybes l))

clearLines rows
  | empty rows = replicate (missing_rows rows) empty_row ++ remove_lines rows
  | otherwise = rows

missing_rows rows = length rows - length (remove_lines rows)
empty_row = replicate 10 Nothing
remove_lines = filter $ not . fullLine

fullLine :: Row -> Bool
fullLine line = filter (/= Nothing) line == line

freezeBlocks rows
  | stopped rows = map freezeBlocks' rows
  | otherwise = rows
  where
    freezeBlocks' (Just (Block s True o):t) = Just (Block s False o) : freezeBlocks' t
    freezeBlocks' b = head b:freezeBlocks' (tail b)

createShape sh
  | sh == I = pad createI
  | sh == J = pad createJ
  | sh == L = pad createL
  | sh == S = pad createS
  | sh == Z = pad createZ
  | sh == O = pad createO
  | sh == T = pad createT
  | otherwise = error "Error: createShape"
  where
    block shape' origin' = Just $ Block shape' True origin'
    x = Nothing
    hpad l = replicate 3 x ++ l ++ replicate 4 x

    pad s
      | length s == 2 = empty_row : map hpad s ++ [empty_row]
      | length s == 3 = empty_row : map hpad s
      | otherwise = map hpad s

    createI = [
        [x,b,x],
        [x,o,x],
        [x,b,x],
        [x,b,x]
      ]
      where
        b = block I False
        o = block I True

    createJ = [
      [x,b,x],
      [x,o,x],
      [b,b,x]
              ]
      where
        b = block J False
        o = block J True

    createL = [
      [x,b,x],
      [x,o,x],
      [x,b,b]
              ]
      where
        b = block L False
        o = block L True

    createS = [
      [x,b,b],
      [b,o,x]
              ]
      where
        b = block S False
        o = block S True

    createZ = [
      [b,b,x],
      [x,o,b]
              ]
      where
        b = block Z False
        o = block Z True

    createO = [
      [x,b,b],
      [x,b,b]
              ]
      where
        b = block O False

    createT = [
      [b,o,b],
      [x,b,x]
              ]
      where
        b = block T False
        o = block T True

module TetrisCurses where

import Control.Monad
import Data.Char
import Data.List
import System.Random
import Tetris
import Text.Printf
import UI.NCurses

playGeme theHightScores = newStdGen >>= \g -> runCurses $ do
  w <- defaultWindow
  gridcolor <- newColorID ColorBlue ColorDefault 1
  red <- newColorID ColorRed ColorRed 2
  green <- newColorID ColorGreen ColorGreen 3
  blue <- newColorID ColorBlue ColorBlue 4
  yellow <- newColorID ColorYellow ColorYellow 5
  cyan <- newColorID ColorCyan ColorCyan 6
  white <- newColorID ColorWhite ColorWhite 7
  magenta <- newColorID ColorMagenta ColorMagenta 8
  redtext <- newColorID ColorRed ColorDefault 9

